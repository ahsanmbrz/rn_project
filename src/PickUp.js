import React, { Component } from "react";
import { StyleSheet, Text, View, Button} from "react-native";


export default class PickUp extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }
  scanQRCodeAgain() {
    this.props.navigation.navigate("PassengerScannerScreen");
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Start Scanning Passenger</Text>
        
        <Button
          title={"Add Passenger"}
          onPress={() => this.scanQRCodeAgain()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  text: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  }
});