import React, { Component } from "react";
import {View,Text,StyleSheet,Dimensions, PermissionsAndroid, AsyncStorage, TouchableOpacity,ToastAndroid, ActivityIndicator} from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import Global from "./Global";
import BackgroundGeolocation from '@mauron85/react-native-background-geolocation';
import moment from 'moment';  
import 'moment/locale/id';

const {width,height} = Dimensions.get('window');
const GOOGLE_MAPS_APIKEY = 'AIzaSyDb22AAYakSPDYGOCXLsxIkOAb__ARp5Og';

const ASPECT_RATIO = width / height;

export default class NonScheduled extends Component {
  constructor(props) {
    super(props);

    this.state = { 
      NonScheduledData:null, 
      scanner: undefined,
      bus:'5d2ec23ef9bf2933cc7f30c4',
      region: {
            latitude: "",
            longitude: "",
            latitudeDelta:"",
            longitudeDelta:"",
            accuracy:""},
      destination:{
            latitude:null,
            longitude:null},
      initialData:{
            latitude:null,
            longitude:null
      },
      navigate:false,
      choosedest:true,
      tempcoord:{
            latitude:null,
            longitude:null
      },
      recordid:null
    }  
  }
 componentWillMount(){
  const NonScheduledData = this.props.navigation.getParam("data", null);
  const scanner = this.props.navigation.getParam("scanner", () => false);
  fetch(Global.BASE_URL+"finduser", {
                method: 'POST',
                body: JSON.stringify({
                  bus: this.state.bus,
                  passid:NonScheduledData
                }),
                headers: {
                  "Content-type": "application/json"
                }
              }).then(res => res.json())
                .then(
                  (result) => {
                    if(result.status=="failed"){
                      ToastAndroid.show('Validity Failed', ToastAndroid.SHORT);
                      this.props.navigation.popToTop();
                    }else{
                    ToastAndroid.show('valid', ToastAndroid.SHORT);
                    this.setState({ NonScheduledData: NonScheduledData, scanner: scanner });
                    this.configureGeoTracking();
                    }
                  },
                  (error) => {
                    console.log(error);
                  }
                )

 }
  marker(){
      return {
          latitude:this.state.region.latitude,
          longitude:this.state.region.longitude
      }
  }
  dirmarker(){
      return {
          latitude:this.state.initialData.latitude,
          longitude:this.state.initialData.longitude
      }
  }
 calDelta(lat,long,accuracy){
    const oneDegreeOfLatitudeInMeters = 111.32 * 1000;
       
    //    const latDelta =accuracy / oneDegreeOfLatitudeInMeters;
    //    const longDelta = accuracy / (oneDegreeOfLatitudeInMeters * Math.cos(lat * (Math.PI / 180)));
    
    const longDelta=0.0322;
    const latDelta = longDelta*ASPECT_RATIO;

        this.setState({
            region:{
              latitude:lat,
              longitude:long,
              latitudeDelta:latDelta,
              longitudeDelta:longDelta,
              accuracy:accuracy,
              },
            });
    }
  configureGeoTracking() {
    BackgroundGeolocation.configure({
      desiredAccuracy: BackgroundGeolocation.HIGH_ACCURACY,
      stationaryRadius: 70,
      distanceFilter: 70,
      notificationTitle: 'Background tracking',
      notificationText: 'enabled',
      debug: true,
      startOnBoot: false,
      notificationsEnabled: true,
      locationProvider: BackgroundGeolocation.ACTIVITY_PROVIDER,
      interval: 10000,
      fastestInterval: 10000,
      activitiesInterval: 15000,
      stopOnTerminate:false,
      stopOnStillActivity: false,
      // fastestInterval: 5000,
      // activitiesInterval: 10000,
      // stopOnStillActivity: false,                              
      // url: SERVER_BASE_URL+'locations',                                                  //PENTING // NULL DULU, contoh nilai : 'http://192.168.81.15:3000/location'
      // syncUrl: SERVER_BASE_URL+'sync',                                              //PENTING
      // syncThreshold: 100,
      httpHeaders: {                                              //PENTING
        'X-FOO': 'bar'
      },
      // customize post properties
      postTemplate: {                                             //PENTING
        lat: '@latitude',
        lon: '@longitude',
        foo: 'bar' // you can also add your own properties
      }
    })

    BackgroundGeolocation.on('location', (location) => {
      // handle your locations here
      // to perform long running operation on iOS
      // you need to create background task
      BackgroundGeolocation.startTask(taskKey => {
        moment.locale('id')
        const currentDate = moment().toDate()  
        const last = {
        // id: location.id,
          _id: this.state.bus,
          latitude: location.latitude,
          longitude: location.longitude,
          altitude: location.altitude,
          time: location.time,
          accuracy: location.accuracy,
          action: 'nojob',
          // synced: false,
          created: currentDate,
          ...location,
          activity: 'nothing',
          username:"baco"
        }
        const lat = location.latitude;
        const long = location.longitude;
        const accuracy = location.accuracy;       
        this.calDelta(lat,long,accuracy);
         if(this.state.initialData.longitude==null){
          this.setState({
            initialData:{
              latitude:location.latitude,
              longitude:location.longitude
            }
          });
          console.log('datainit',this.state.initialData);
        }
        // console.log('lokasinya [extd]', last)
        this.storeLocation(last);
       
        // execute long running task
        // eg. ajax post location
        // IMPORTANT: task has to be ended by endTask
        BackgroundGeolocation.endTask(taskKey);
      });
        // BackgroundGeolocation.startTask(taskKey => {
        // // execute long running task
        // // eg. ajax post location
        // // IMPORTANT: task has to be ended by endTask
        //   BackgroundGeolocation.endTask(taskKey)
        // })

    })

    BackgroundGeolocation.on('stationary', (stationaryLocation) => {
      BackgroundGeolocation.startTask(taskKey => {
          moment.locale('id')
          const currentDate = moment().toDate()  
          const last = {  
          // id: location.id,
            _id: this.state.bus,
            latitude: stationaryLocation.latitude,
            longitude: stationaryLocation.longitude,
            altitude: stationaryLocation.altitude,
            time: stationaryLocation.time,
            accuracy: stationaryLocation.accuracy,
            action: 'nojob',
            // synced: false,
            created: currentDate,
            ...stationaryLocation,
            activity: 'nothing',
            username:"baco"
          }      
          const lat = stationaryLocation.latitude;
          const long = stationaryLocation.longitude;
          const accuracy = stationaryLocation.accuracy;
                
          this.calDelta(lat,long,accuracy);
          this.storeLocation(last);
          if(this.state.initialData.latitude==null){
          this.setState({
            initialData:{
              latitude:stationaryLocation.latitude,
              longitude:stationaryLocation.longitude
            }
          });
          console.log('datainit',this.state.initialData);
        }
          console.log('stationaryLocation', stationaryLocation);
      BackgroundGeolocation.endTask(taskKey);
    });
  });

    BackgroundGeolocation.on('error', (error) => {
      console.log('[ERROR] BackgroundGeolocation error:', error)
    })

    BackgroundGeolocation.on('start', () => {
      console.log('[INFO] BackgroundGeolocation service has been started')  

    })

    BackgroundGeolocation.on('stop', () => {
      console.log('[INFO] BackgroundGeolocation service has been stopped')
    })

    BackgroundGeolocation.on('authorization', (status) => {
      console.log('[INFO] BackgroundGeolocation authorization status: ' + status)
      if (status !== BackgroundGeolocation.AUTHORIZED) {
        // we need to set delay or otherwise alert may not be shown
        setTimeout(() =>
          Alert.alert('App requires location tracking permission', 'Would you like to open app settings?', [
            { text: 'Yes', onPress: () => BackgroundGeolocation.showAppSettings() },
            { text: 'No', onPress: () => console.log('No Pressed'), style: 'cancel' }
          ]), 1000)
      }
    })

    BackgroundGeolocation.on('background', () => {
      console.log('[INFO] App is in background')
    })

    BackgroundGeolocation.on('foreground', () => {
      console.log('[INFO] App is in foreground')
    })

    // BackgroundGeolocation.on('abort_requested', () => {
    //   console.log('[INFO] Server responded with 285 Updates Not Required')

    //   // Here we can decide whether we want stop the updates or not.
    //   // If you've configured the server to return 285, then it means the server does not require further update.
    //   // So the normal thing to do here would be to `BackgroundGeolocation.stop()`.
    //   // But you might be counting on it to receive location updates in the UI, so you could just reconfigure and set `url` to null.
    // })

    BackgroundGeolocation.on('http_authorization', () => {
      console.log('[INFO] App needs to authorize the http requests')
    })

    // BackgroundGeolocation.on('activity', activity => {
    //   console.log('activity', activity)
    //   console.log('act event', activity)    
    //   Toast.show({
    //     text: JSON.stringify(activity)
    //   })
    //   this.props.setActivity(activity)
    // })

    BackgroundGeolocation.checkStatus(status => {
      
      console.log('[INFO] BackgroundGeolocation service is running', status.isRunning)
      console.log('[INFO] BackgroundGeolocation services enabled', status.locationServicesEnabled)
      console.log('[INFO] BackgroundGeolocation auth status: ' + status.authorization)
        if (!status.isRunning) {
          BackgroundGeolocation.start(); //triggers start on start event
        }
    })
    // you can also just start without checking for status
    // BackgroundGeolocation.start();
    // BackgroundGeolocation.checkStatus(status => {
    //   this.props.setActive(status.isRunning)
    // })
  }
  storeLocation(hasil){
    var result=JSON.stringify(hasil);
    
    console.log(result);
    fetch(Global.LOC_URL+"locationlistener", {
      method: 'POST',
      body: JSON.stringify([hasil]),
      headers: {
        "Content-type": "application/json"
      }
    }).then(res => res.json())
      .then(
        (result) => {
          if(result.status=="no data"){
            ToastAndroid.show('Validity Failed', ToastAndroid.SHORT);
          }else{
          console.log(result),
          ToastAndroid.show('list show', ToastAndroid.SHORT);
        }
        },
        (error) => {
          console.log(error);
        }
      )
  }
  handledraggable(e){
    this.setState({
      tempcoord:{
        latitude:e.latitude,
        longitude:e.longitude
      }
    })
  }
  chooseend(){
    this.setState({
      destination:{
        latitude:this.state.tempcoord.latitude,
        longitude:this.state.tempcoord.longitude
      }
    })
    fetch(Global.BASE_URL+"createrecord", {
                method: 'POST',
                body: JSON.stringify({
                  bus: this.state.bus,
                  passenger:this.state.NonScheduledData,
                  latitude:this.state.tempcoord.latitude,
                  longitude:this.state.tempcoord.longitude
                }),
                headers: {
                  "Content-type": "application/json"
                }
              }).then(res => res.json())
                .then(
                  (result) => {
                    if(result.status=="failed"){
                      ToastAndroid.show('Validity Failed', ToastAndroid.SHORT);
                      this.props.navigation.popToTop();
                    }else{
                    this.setState({
                      recordid:result.id,
                      navigate:true,
                      choosedest:false,
                    })

                    ToastAndroid.show('valid', ToastAndroid.SHORT);
                    }
                  },
                  (error) => {
                    console.log(error);
                  }
                )
  }
  finish(){
    fetch(Global.BASE_URL+"finished", {
                method: 'POST',
                body: JSON.stringify({
                  scheduleid: this.state.recordid
                }),
                headers: {
                  "Content-type": "application/json"
                }
              }).then(res => res.json())
                .then(
                  (result) => {
                    if(result.status=="success"){
                      ToastAndroid.show('success', ToastAndroid.SHORT);
                      BackgroundGeolocation.stop();
                      BackgroundGeolocation.removeAllListeners();
                      this.props.navigation.popToTop();
                    }else{

                    ToastAndroid.show('notvalid', ToastAndroid.SHORT);
                    }
                  },
                  (error) => {
                    console.log(error);
                  }
                )
            
  }

  render(){
        console.log(this.state.region)
        return(
              <View style={styles.container}> 
              {
                this.state.initialData.latitude ? 
                <MapView 
                 style={styles.map}
                 initialRegion={this.state.region}
                  >
                  {this.state.choosedest &&
                   <MapView.Marker draggable
                      coordinate={this.dirmarker()}
                      onDragEnd={(e) => this.handledraggable(e.nativeEvent.coordinate)}
                      title="Choose"
                      description="choose destination"
                      pinColor='blue'
                   />
                 //   <TouchableOpacity
                 //    onPress = {() => this.chooseend()}
                 //    style={{  
                 //        position: 'absolute',
                 //        bottom: 5,
                 //        right:10,
                 //        height:40,
                 //        width:160,
                 //        alignItems: 'center',
                 //        justifyContent: 'center',
                 //        backgroundColor: '#ee6e73',
                 //        borderRadius: 10
                 //    }}>
                 //   <Text>Set Destination</Text>
                 // </TouchableOpacity>
               }
               {this.state.navigate && 
                  <MapView.Marker 
                    coordinate={this.marker()}
                    title="You"
                    description="You are here!"
                    pinColor='red'/>
               }
               {this.state.navigate && 
                 <MapView.Marker 
                    coordinate={this.state.destination}
                    title="You"
                    description="You are here!"
                    pinColor='green'/>
               }
               {this.state.navigate && 
                 <MapViewDirections
                        origin={this.dirmarker()}
                        destination={this.state.destination}
                        apikey={GOOGLE_MAPS_APIKEY}
                        strokeWidth={3}
                        strokeColor="hotpink"
                />
                 }
                         
                </MapView>
                : <ActivityIndicator size="large" color="#0000ff" /> 
              }
              {this.state.choosedest &&
                   <TouchableOpacity
                    onPress = {() => this.chooseend()}
                    style={{  
                        position: 'absolute',
                        bottom: 5,
                        right:10,
                        height:40,
                        width:160,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: '#ee6e73',
                        borderRadius: 10
                    }}>
                   <Text>Set Destination</Text>
                 </TouchableOpacity>
               }
               {this.state.navigate &&
               <TouchableOpacity
                    onPress = {() => this.finish()}
                    style={{  
                        position: 'absolute',
                        bottom: 5,
                        right:10,
                        height:40,
                        width:160,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: '#ee6e73',
                        borderRadius: 10
                    }}>
                     <Text>finish</Text>
                   </TouchableOpacity>
                 }
              </View>
           )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center'
    },
    map: {
        width:width,
        height:height,
        flex: 1
        }
});
