import React, { Component } from "react";
import { StyleSheet, Text, View, Button,AsyncStorage,ToastAndroid,TouchableOpacity } from "react-native";
import Global from "./Global"


export default class Passenger extends Component {
  constructor(props) {
    super(props);

    this.state = { qrCodeData:null, scanner: undefined,jadwalid:null,list:[] };
  }
 
  componentDidMount() {
    //The code bellow will receive the props passed by QRCodeScannerScreen

    const qrCodeData = this.props.navigation.getParam("data", null);
    const scanner = this.props.navigation.getParam("scanner", () => false);
    this.setState({ qrCodeData: qrCodeData, scanner: scanner });
    AsyncStorage.getItem('jadwalid', (error, hasil) => {
        if (hasil) {
            this.setState({jadwalid: hasil});
            if(this.state.qrCodeData!==null){
              fetch(Global.BASE_URL+"insertpass", {
                method: 'POST',
                body: JSON.stringify({
                  jadwalid: this.state.jadwalid,
                  id:this.state.qrCodeData
                }),
                headers: {
                  "Content-type": "application/json"
                }
              }).then(res => res.json())
                .then(
                  (result) => {
                    if(result.status=="failed"){
                      ToastAndroid.show('insert Failed', ToastAndroid.SHORT);
                    }else{
                    ToastAndroid.show('inserting', ToastAndroid.SHORT);
                    this.showlistpassenger(hasil);
                    }
                  },
                  (error) => {
                  }
                )
            }
        }
      });
  }

  scanQRCodeAgain() {
    this.state.scanner.reactivate();
    this.props.navigation.goBack();
  }
  showlistpassenger(result){
    fetch(Global.BASE_URL+"showschedule", {
			method: 'POST',
			body: JSON.stringify({
        jadwalid: result
			}),
			headers: {
				"Content-type": "application/json"
			}
    }).then(res => res.json())
      .then(
        (result) => {
          if(result.status=="failed"){
            ToastAndroid.show('picking up Failed', ToastAndroid.SHORT);
          }else{
              this.setState({
                  list:result.result.passenger
              })
          ToastAndroid.show('picking up', ToastAndroid.SHORT);
        }
        },
        (error) => {
        }
      )
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>{this.state.qrCodeData}</Text>
        <Button
          title={"Scan QRCode Again"}
          onPress={() => this.scanQRCodeAgain()}
        />
         <Text style={styles.text}>List Passenger</Text>
        { this.state.list.map((item, key)=>(
         <Text key={key}> id: {item._id } status:{item.status}</Text>)
         )}
         <TouchableOpacity
            onPress = {() => this.props.navigation.navigate('ToDestinationMaps')}
            style={{ 
                position: 'absolute',
                bottom: 5,
                right:10,
                height:40,
                width:160,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: '#ee6e73',
                borderRadius: 30
            }}
            >
            <Text>Start Navigation</Text>
         </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  text: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  }
});
