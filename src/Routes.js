import { createAppContainer, createStackNavigator } from "react-navigation";

import Home from "./Home";
import QRCodeScannerScreen from "./QRCodeScannerScreen";
import QRCodeData from "./QRCodeData";
import PickUp from "./PickUp";
import PassengerScannerScreen from "./PassengerScannerScreen";
import Passenger from "./Passenger";
import ToPickUpMaps from "./ToPickUpMaps";
import ToDestinationMaps from "./ToDestinationMaps";
import NonScheduledScannerScreen from "./NonScheduledScannerScreen";
import NonScheduled from "./NonScheduled"; // <--- New Line

const mainStack = createStackNavigator(
  {
    Home: Home,
    QRCodeScannerScreen: QRCodeScannerScreen,
    QRCodeData: QRCodeData,
    PickUp:PickUp,
    PassengerScannerScreen:PassengerScannerScreen,
    Passenger:Passenger,
    ToPickUpMaps:ToPickUpMaps,
    ToDestinationMaps:ToDestinationMaps,
    NonScheduledScannerScreen:NonScheduledScannerScreen,
    NonScheduled:NonScheduled
    // <--- New line
  },
  { defaultNavigationOptions: { header: null } }
);

const AppContainer = createAppContainer(mainStack);

export default AppContainer;
