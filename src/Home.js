import React, { Component } from "react";
import { StyleSheet, Text, View, TouchableOpacity, ToastAndroid, Alert, AsyncStorage,Button, PermissionsAndroid, BackHandler} from "react-native";
import Global from "./Global";
import BackgroundGeolocation from '@mauron85/react-native-background-geolocation';
import moment from 'moment';
import 'moment/locale/id';


export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = { list:[],granted:null,bus:'5d2ec23ef9bf2933cc7f30c4'};
  }
 componentDidMount(){
   this.permissionPromptAndBackgroundConfig();
  //  await this.requestLocationPermission();
    fetch(Global.BASE_URL+"schedule_bus", {
			method: 'POST',
			body: JSON.stringify({
        bus: "5d2ec23ef9bf2933cc7f30c4"
			}),
			headers: {
				"Content-type": "application/json"
			}
    }).then(res => res.json())
      .then(
        (result) => {
          if(result.status=="no data"){
            ToastAndroid.show('Validity Failed', ToastAndroid.SHORT);
          }else{
          console.log(result),
          ToastAndroid.show('list show', ToastAndroid.SHORT);
          this.setState({
            list: result.result.hasil
          });
        }
        },
        (error) => {
        }
      )
  }
  showid=(id)=>{
    Alert.alert(
      'Confirmation',
      'Are you sure to pick up this schedule?',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: () => {
          AsyncStorage.setItem('jadwalid', id);
          this.pickup(id);
          this.topickupmaps();
        }
      },
      ],
      {cancelable: false},
    );
  }
  topickupmaps(){
    BackgroundGeolocation.removeAllListeners();
    BackgroundGeolocation.stop();
    this.props.navigation.navigate("ToPickUpMaps");
  }
  pickup(id){
    fetch(Global.BASE_URL+"pickup", {
			method: 'POST',
			body: JSON.stringify({
        jadwalid: id
			}),
			headers: {
				"Content-type": "application/json"
			}
    }).then(res => res.json())
      .then(
        (result) => {
          if(result.status=="failed"){
            ToastAndroid.show('picking up Failed', ToastAndroid.SHORT);
          }else{
          ToastAndroid.show('picking up', ToastAndroid.SHORT);
        }
        },
        (error) => {
        }
      )
  }
   permissionPromptAndBackgroundConfig() {
    try {
      const result =   PermissionsAndroid.requestMultiple(
        [
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
          PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE,
          // PermissionsAndroid.PERMISSIONS.BODY_SENSORS,
        ]
      )
      console.log('result', result);
      this.configureGeoTracking();
      if (result['android.permission.READ_PHONE_STATE'] === PermissionsAndroid.RESULTS.GRANTED &&
        // result.android.permission.BODY_SENSORS === PermissionsAndroid.RESULTS.GRANTED &&
        result['android.permission.ACCESS_COARSE_LOCATION'] === PermissionsAndroid.RESULTS.GRANTED &&
        result['android.permission.ACCESS_FINE_LOCATION'] === PermissionsAndroid.RESULTS.GRANTED) {
        this.setState({
          granted: true
        })
        console.log(result['android.permission.READ_PHONE_STATE'])
        
        // this.startActivityRecognition()
      } else {
        this.setState({
          granted: false
        })
      }
    } catch (err) {
      console.warn(err)
    }
  }

  configureGeoTracking() {
    BackgroundGeolocation.configure({
      desiredAccuracy: BackgroundGeolocation.HIGH_ACCURACY,
      stationaryRadius: 10,
      distanceFilter: 10,
      notificationTitle: 'Background tracking',
      notificationText: 'enabled',
      debug: true,
      startOnBoot: false,
      notificationsEnabled: true,
      locationProvider: BackgroundGeolocation.ACTIVITY_PROVIDER,
      interval: 7000,
      fastestInterval: 5000,
      activitiesInterval: 10000,
      stopOnTerminate:false,
      stopOnStillActivity: false,
      httpHeaders: {                                              //PENTING
        'X-FOO': 'bar'
      },
      // customize post properties
      postTemplate: {                                             //PENTING
        lat: '@latitude',
        lon: '@longitude',
        foo: 'bar' // you can also add your own properties
      }
    })

    BackgroundGeolocation.on('location', (location) => {
      // handle your locations here
      // to perform long running operation on iOS
      // you need to create background task
      BackgroundGeolocation.startTask(taskKey => {
        moment.locale('id')
        const currentDate = moment().toDate()  
        const last = {
        // id: location.id,
          _id: this.state.bus,
          latitude: location.latitude,
          longitude: location.longitude,
          altitude: location.altitude,
          time: location.time,
          accuracy: location.accuracy,
          action: 'nojob',
          // synced: false,
          created: currentDate,
          ...location,
          activity: 'nothing',
          username:"baco"
        }
        // console.log('lokasinya [extd]', last)
        this.storeLocation(last);
        // execute long running task
        // eg. ajax post location
        // IMPORTANT: task has to be ended by endTask
        BackgroundGeolocation.endTask(taskKey);
      });
        // BackgroundGeolocation.startTask(taskKey => {
        // // execute long running task
        // // eg. ajax post location
        // // IMPORTANT: task has to be ended by endTask
        //   BackgroundGeolocation.endTask(taskKey)
        // })

    })

    BackgroundGeolocation.on('stationary', (stationaryLocation) => {
      // handle stationary locations here
      // Actions.sendLocation(stationaryLocation);
      // Toast.show({
      //   text: JSON.stringify(stationaryLocation)
      // })

      BackgroundGeolocation.startTask(taskKey => {
        moment.locale('id')
        const currentDate = moment().toDate()  
        const last = {
        // id: location.id,
          _id: this.state.bus,
          latitude: stationaryLocation.latitude,
          longitude: stationaryLocation.longitude,
          altitude: stationaryLocation.altitude,
          time: stationaryLocation.time,
          accuracy: stationaryLocation.accuracy,
          action: 'nojob',
          // synced: false,
          created: currentDate,
          ...stationaryLocation,
          activity: 'nothing',
          username:"baco"
        }      
        this.storeLocation(last);
        console.log('stationaryLocation', stationaryLocation)
        BackgroundGeolocation.endTask(taskKey);
      });
    })

    BackgroundGeolocation.on('error', (error) => {
      console.log('[ERROR] BackgroundGeolocation error:', error)
    })

    BackgroundGeolocation.on('start', () => {
      console.log('[INFO] BackgroundGeolocation service has been started')  

    })

    BackgroundGeolocation.on('stop', () => {
      console.log('[INFO] BackgroundGeolocation service has been stopped')
    })

    BackgroundGeolocation.on('authorization', (status) => {
      console.log('[INFO] BackgroundGeolocation authorization status: ' + status)
      if (status !== BackgroundGeolocation.AUTHORIZED) {
        // we need to set delay or otherwise alert may not be shown
        setTimeout(() =>
          Alert.alert('App requires location tracking permission', 'Would you like to open app settings?', [
            { text: 'Yes', onPress: () => BackgroundGeolocation.showAppSettings() },
            { text: 'No', onPress: () => console.log('No Pressed'), style: 'cancel' }
          ]), 1000)
      }
    })

    BackgroundGeolocation.on('background', () => {
      console.log('[INFO] App is in background')
    })

    BackgroundGeolocation.on('foreground', () => {
      console.log('[INFO] App is in foreground')
    })

    // BackgroundGeolocation.on('abort_requested', () => {
    //   console.log('[INFO] Server responded with 285 Updates Not Required')

    //   // Here we can decide whether we want stop the updates or not.
    //   // If you've configured the server to return 285, then it means the server does not require further update.
    //   // So the normal thing to do here would be to `BackgroundGeolocation.stop()`.
    //   // But you might be counting on it to receive location updates in the UI, so you could just reconfigure and set `url` to null.
    // })

    BackgroundGeolocation.on('http_authorization', () => {
      console.log('[INFO] App needs to authorize the http requests')
    })

    // BackgroundGeolocation.on('activity', activity => {
    //   console.log('activity', activity)
    //   console.log('act event', activity)    
    //   Toast.show({
    //     text: JSON.stringify(activity)
    //   })
    //   this.props.setActivity(activity)
    // })

    BackgroundGeolocation.checkStatus(status => {
      
      console.log('[INFO] BackgroundGeolocation service is running', status.isRunning)
      console.log('[INFO] BackgroundGeolocation services enabled', status.locationServicesEnabled)
      console.log('[INFO] BackgroundGeolocation auth status: ' + status.authorization)
        if (!status.isRunning) {
          BackgroundGeolocation.start(); //triggers start on start event
        }
    })
    // you can also just start without checking for status
    // BackgroundGeolocation.start();
    // BackgroundGeolocation.checkStatus(status => {
    //   this.props.setActive(status.isRunning)
    // })
  }
  // componentWillUnmount() {
  //   // unregister all event listeners
    
    
  // }
  getmeout(){
    BackgroundGeolocation.removeAllListeners();
    BackgroundGeolocation.stop();
    BackHandler.exitApp();
  }
  toNonScheduled(){
     BackgroundGeolocation.removeAllListeners();
    BackgroundGeolocation.stop();
    this.props.navigation.navigate("NonScheduledScannerScreen");
  }
  storeLocation(hasil){
    var result=JSON.stringify(hasil);
    
    console.log(result);
    fetch(Global.LOC_URL+"locationlistener", {
			method: 'POST',
			body: JSON.stringify([hasil]),
			headers: {
				"Content-type": "application/json"
			}
    }).then(res => res.json())
      .then(
        (result) => {
          if(result.status=="no data"){
            ToastAndroid.show('Validity Failed', ToastAndroid.SHORT);
          }else{
          console.log(result),
          ToastAndroid.show('list show', ToastAndroid.SHORT);
        }
        },
        (error) => {
        }
      )
  }
// async requestLocationPermission() {
//     try {
//       const granted = await PermissionsAndroid.request(
//         PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
//         {
//           'title': 'Location Permission',
//           'message': 'This App needs access to your location ' +
//                      'so we can know where you are.'
//         }
//       )
//       if (granted === PermissionsAndroid.RESULTS.GRANTED) {
//         console.log("You can use locations ")
//       } else {
//         console.log("Location permission denied")
//       }
//     } catch (err) {
//       console.warn(err)
//     }
//   }
  
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Jadwal Pengantaran Hari Ini</Text>
        { this.state.list.map((item, key)=>(
         <TouchableOpacity
         key = {key}
         style = {styles.listku}
         onPress = {() => this.showid(item._id)}>
         <Text style={
           {
             fontWeight:'bold'
           }
         }>Titik Jemput:{item.pickuploc.name}</Text>
         <Text style={
           {
             fontWeight:'bold'
           }
         }> Kontingen:{item.kontingen}</Text>
         <Text>Jadwal Jemput :{item.jadwal}</Text>
        </TouchableOpacity>
         ))}
        <Button
          title={"NonScheduled"}
          onPress={() => this.toNonScheduled()}
        />
        <Button
          title={"exit"}
          onPress={() => this.getmeout()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    marginTop:40,
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  listku: {
    padding: 10,
    marginTop: 3,
    backgroundColor: '#d9f9b1',
    width:"90%",
    borderWidth:1,
    borderColor: 'black',
    borderRadius: 10
 },
  text: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  }
});
